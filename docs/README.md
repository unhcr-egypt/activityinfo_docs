# ActivityInfo Reporting Guide
### Egypt 2020

[ActivityInfo](https://activityinfo.org) is an online system that allows users to store, share and analyze data. Using ActivityInfo, we aim to harmonize reporting and data collection, since using Excel sheets is extremely time-consuming and vulnerable to errors such as duplication or missed values. 

This document details the steps required from sector partners to be able to report on the **Program** and **3RP** databases on ActivityInfo. 

Organizations report to all sectors by submitting data once a month. For any clarification needed about this document please contact program or respective sector focal points.


![ActivityInfo](./images/001_ai.png)