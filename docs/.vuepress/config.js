module.exports = {
	title: 'Activityinfo: Data Entry Guide',
	description: 'IM Datasets: Egypt running on GitLab Pages',
	base: '/activityinfo_docs/',
	dest: 'public',


	themeConfig: {
		nav: [
			{ text: 'Home', link: '/' },
			{ text: 'Contact Us', link: '/contact/' },
		],

		sidebar: [
			['/', 'Home'],
			['GettingStarted', 'Getting Started'],
			['HowToEnter', 'Data Entry'],
			['furtherresources','Further Resources']
		]

	}
}
