# How to Enter Data

Reporting is done once a month, covering the achievements of the month before. Please consult your sector focal point for the reporting date.

Select a relevant **Database** for **Data Entry**

Each Sector/Activity Category is categorized under the respective folders. Within the folder you will find a unique set of outcomes, outputs, and activity indicators. If your organization is working on more than one type of intervention, you will report each intervention against its specific indicator.

```2020 Egypt ``` : **Program Database**

```Egypt 3RP 2020``` :**3RP Database**

:warning: Before you begin, consult your sector-specific guide to indicators.

![Database](./images/003-ai_database.png)

## Step 1: Setting up record 
Once you are in the Data Entry Interface, open the relevant database by clicking on the arrow to the left of the database title.

Indicators in the **3RP database** are grouped into the respective sector folder whereas in the **Program database** the outcomes/outputs are grouped under activity category folder.

![Database](./images/005_ai_database_2_1_1-0.png)


![Database](./images/005_ai_database_2_2.png)


:warning: Consult your sector guide if you cannot find the appropriate indicator in the list.

1. Select the relevant form.
2. Skip to **Step 3**, if you have created a **record** previously.
3. Otherwise, Click ▶️

:point_up: This is a one time process for one indicator per location for your organisation.

![DataEntry](./images/006_ai_dataentry_1.png)

## Step 2: Indicate Location

Clicking **New record**  may prompt the Choose Site window. This will appear if it is necessary to specify the specific site of the intervention (e.g. an informal settlement, public health centre). 

1. Select InterventionDetails: Select the partner agency you are working for
2. Select **Project**, if relevant
3. Select the location in which you are implementing the selected intervention.
4. To proceed click on **Save** at the bottom of the tab.

![DataEntry](./images/007_ai_location.png)


## Step 3: Report Monthly Achievement

1. Select the relevant site that you created in the previous step and Click on the **Add Record** button  to open data entry form.
3. After inputting the indicator values for that particular month and site, press **Save record** on the tab. 


![Monthly Report](./images/009_ai_monthly_01.png)


![Monthly Report](./images/009_ai_monthly_02.png)