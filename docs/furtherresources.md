# Further Resources

Find other useful resources on ActivityInfo.

* [ActivityInfo Data Entry Training Slides](https://gitlab.com/rvibek/activityinfo_docs/-/raw/master/docs/images/ActivityInfo_2020_Training.pdf)

* [ActivityInfo Documentation](http://help.activityinfo.org)
* [ActivityInfo Webinars](http://help.activityinfo.org/m/tutorials_and_webinars)
* [Setting up ActivityInfo and Power BI](http://help.activityinfo.org/m/84880/l/1111841-guide-to-setting-up-activityinfo-and-power-bi)
