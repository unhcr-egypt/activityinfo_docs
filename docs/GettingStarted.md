# Getting Started


In Egypt, monitoring and reporting is done through [ActivityInfo](https://activityinfo.org), an online monitoring tool. Egypt maintains two databases - for ```Program``` and ```3RP``` related activities. 

Each database allows partners to store, access, manage, map and analyze data on defined indicators, and thus to monitor projects/activities.

## Accessing ActivityInfo

If you already have an account, login to [ActivityInfo](http://activityinfo.org/login).

![Login](./images/002_ai_login.png)


:warning: If you do not yet have an accont, [sign up](https://www.activityinfo.org/signUp) to registerd


## Request Access to Database
In order to access the database(s) related to your projects, you need to be included in the particular database. Usually, the members are added to the database as recommended by sector focal points. 

If you do not have access to the database then contact your **focal point or sector lead**.

![Database](./images/003-ai_database.png)


## ActivityInfo Screen

There are four main sections of ActivityInfo:

1. **Data Entry:** Where you will enter data to report by clicking on the relevant database.

2. **Databases:** Displays the list of database available.

3. **Reports:** For generating basic chartsa and pivot tables.

4. **Search Bar:**  Access the form by typing name or a part of the form name.

Based on your access rights, the activities could be limited. 

![UI](./images/004_ai_UI.png)

This guide focuses on **Data Entry**

For more help, please refer to [Activity Documentation](http://help.activityinfo.org/).

